## Installation

Follow these steps to set up and run the project locally.

### Prerequisites

Make sure you have the following installed on your local machine:

- Node.js (v12 or higher)
- npm (v6 or higher)

### Steps


1. **Install dependencies:**

```sh

npm install
```

2. **Start the development server:**

```sh

npm start


La realisation de ce projet a suivi les metohodes agile  afin d'etre proactif: 

https://lucid.app/lucidchart/58fe585c-0f50-46e0-861d-21c40e00ab6b/edit?beaconFlowId=52DD096874FC5771&invitationId=inv_4759797e-e5d4-46fa-9dc5-a68136442cfd&page=0_0

Ci-dessus un tableau organisatiponnel pour la repartition des taches 